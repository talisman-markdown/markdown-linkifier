import * as vscode from "vscode";
import fs = require("fs");
import pathModule = require("path");

function kebabCase(string: string) { //TODO: Other slugification
  return string
    .trim()
    .replace(/\W+/g, "-")
    .toLowerCase()
    .replace(/^-+/, "")
    .replace(/-+$/, "");
}

function createFileIfNotExist(dir: string, filename: string, content :string) {
  filename = filename + ".md";
	content = "# " + content;
  let path = pathModule.join(dir, filename);
  fs.stat(path, (exists) => {
    if (exists === null) {
      return;
    } else if (exists.code === "ENOENT") {
      vscode.window
        .showInformationMessage("File does not exst. Create", "YES", "NO") //TODO: Configure: Always create/Always ask/Do not create or ask
        .then((selection) => {
          if (selection === "YES") {
            fs.writeFile(path, content, function (err) {
              if (err) {
                throw err;
              }
            });
          }
        });
    }
  });
}

export function activate(context: vscode.ExtensionContext) {
  let disposable = vscode.commands.registerCommand("linkifier.linkify", () => {
    let editor = vscode.window.activeTextEditor;
    if (!editor) {
      vscode.window.showErrorMessage("Markdown Linkfier: No active editor");
      return;
    }
    editor.edit((edit: vscode.TextEditorEdit) => {
      if (!editor) {
        return;
      }
      for (let selection of editor.selections) {
        let text: string = editor.document.getText(selection) || "";
        if (text === "") {
          vscode.window.showErrorMessage("Selection is empty");
          return;
        }
        let slug = kebabCase(text);
        let link = "[" + text + "](" + slug + ")";
        edit.replace(selection, link);
        createFileIfNotExist(
          pathModule.dirname(editor.document.uri.fsPath),
          slug,
					text
        );
      }
    });
  });

  context.subscriptions.push(disposable);
}

export function deactivate() {}
